<?php

namespace Deployer;

require_once 'recipe/common.php';
// Project name
set('application', 'pgf-api');

// host
// Staging
host('staging')
  ->hostname('ec2-34-209-5-140.us-west-2.compute.amazonaws.com')
  ->port(22)
  ->forwardAgent(true)
  ->multiplexing(false)
  ->user('symfony')
  ->stage('staging')
  ->set('env', ['SYMFONY_ENV' => 'prod'])
  ->set('env', ['APP_ENV' => 'prod'])
  ->set('deploy_path', '/var/www/pgf-api');

// QA
host('qa')
  ->hostname('ec2-52-13-235-75.us-west-2.compute.amazonaws.com')
  ->port(22)
  ->forwardAgent(true)
  ->multiplexing(false)
  ->configFile('~/.ssh/')
  ->identityFile('~/.ssh/id_rsa')
  ->user('symfony')
  ->stage('qa')
  ->set('env', ['SYMFONY_ENV' => 'prod'])
  ->set('env', ['APP_ENV' => 'prod'])
  ->set('deploy_path', '/var/www/pgf-api');

//Preprod
host('preprod')
  ->hostname('ec2-35-161-87-221.us-west-2.compute.amazonaws.com')
  ->port(22)
  ->forwardAgent(true)
  ->multiplexing(false)
  ->configFile('~/.ssh/')
  ->identityFile('~/.ssh/id_rsa')
  ->user('symfony')
  ->stage('preprod')
  ->set('env', ['SYMFONY_ENV' => 'prod'])
  ->set('env', ['APP_ENV' => 'prod'])
  ->set('deploy_path', '/var/www/pgf-api');

// Prod
host('prod')
  ->hostname('ec2-52-41-78-88.us-west-2.compute.amazonaws.com')
  ->port(22)
  ->forwardAgent(true)
  ->multiplexing(false)
  ->configFile('~/.ssh/')
  ->identityFile('~/.ssh/id_rsa')
  ->user('symfony')
  ->stage('production')
  ->set('env', ['SYMFONY_ENV' => 'prod'])
  ->set('env', ['APP_ENV' => 'prod'])
  ->set('deploy_path', '/var/www/pgf-api');

set('shared_dirs', ['var/log', 'var/sessions']);
set('shared_files', ['.env']);
set('writable_dirs', ['var']);
set('repository', 'git@gitlab.com:akiltech/pgf-api.git');
set('git_tty', false);

set('bin/console', function () {
  return parse('{{bin/php}} {{release_path}}/bin/console --no-interaction');
});
desc('Migrate database');
task('database:migrate', function () {
  run('{{bin/console}} doctrine:migrations:migrate --allow-no-migration');
});
desc('who am i');
task('ask:me', function () {
  run('whoami');
});
desc('Clear cache');
task('deploy:cache:clear', function () {
  run('{{bin/console}} cache:clear --no-warmup');
});
desc('Warm up cache');
task('deploy:cache:warmup', function () {
  run('{{bin/console}} cache:warmup');
});

desc('Deploy project');
task('deploy', [
  'deploy:info',
  'deploy:prepare',
  'deploy:lock',
  'deploy:release',
  'deploy:update_code',
  'deploy:shared',
  'deploy:vendors',
  'deploy:writable',
  'deploy:cache:clear',
  'deploy:cache:warmup',
  'deploy:symlink',
  'database:migrate',
  'deploy:unlock',
  'cleanup',
]);
after('deploy:failed', 'deploy:unlock');
after('deploy', 'success');
