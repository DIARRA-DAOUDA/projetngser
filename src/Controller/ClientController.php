<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Produits;
use App\Form\ClientType;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as SWG;
use App\Repository\ClientRepository;
use App\Repository\ProduitRepository; 
use Lcobucci\JWT\Validation\Constraint;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClientController extends AbstractFOSRestController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    { 
        $this->em=$em; 
    }
    /**
     * @Rest\Get(
     * path = "/client", 
     * name = "list.client")
     * @SWG\Response(
     *      response = 200,
     *      description = "liste des client",
     *      @Model(type=Client::class)
     * )
     * @SWG\Tag(name = "Client")
     * @return View
     */
    public function cgetClientAction(ClientRepository $clientRepository)
    {
        $clients = $clientRepository->findAll();
        return $this->view(['data' => $clients], 200);
    }

    /**
     * @Rest\Post(
     * path = "/client",
     * name = "add.client")
     * @SWG\Response(
     *      response = 200,
     *      description = "ajout de client etat brouillon",
     *      @Model(type=Client::class)
     * )
     * @SWG\Tag(name = "Client")
     * @return View
     */
    public function postClientAction(Request $request, EntityManagerInterface $em)
    {
       
        $requestContent = json_decode($request->getContent(), true);
        $client = $this->addClient(new Client(), $requestContent);
        $errors = $this->validate($client, null, ['Create']);
        if (null !== $errors) {
            return $errors;
        }
        $em->persist($client); 
        $em->flush();
        return $this->view(['data' => $client], 201);
    }

    /**
     * @Rest\Get(
     * path = "/client/{id}", 
     * name = "show.client")
     * @SWG\Response(
     *      response = 200,
     *      description = "voir un client",
     *      @Model(type=Client::class)
     * )
     * @SWG\Tag(name = "Client")
     * @return View
     */
    public function getOneClientAction(string $id, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $client = $em->getRepository(Client::class)->find($id);
        if (!$client) {
            $message = $translator->trans('objet.not_found', ['%id%' => $id], 'errors');
            return new JsonResponse(['message' => $message], 404);
        }
        try {
            return $this->view(['data' => $client], 200);
        } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204);
    }
 

    /**
     * @Rest\Get(
     * path = "/client/{id}", 
     * name = "remove.client")
     * @SWG\Response(
     *      response = 200,
     *      description = "supprimer un client",
     *      @Model(type=Client::class)
     * )
     * @SWG\Tag(name = "Client")
     * @return View
     */
    public function delete(String $id, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $client = $em->getRepository(Client::class)->find($id);
        if (!$client) {
            $message = $translator->trans('objet.not_found', ['%id%' => $client], 'errors');
            return new JsonResponse(['message' => $message], 404);
        }
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
            return new JsonResponse(['message'=>'l\'objet à ete supprimer avec success']);
        } catch (\Exception $e) {
        }
    }

    public function validate(Client $devis, Constraint $constraint = null, array $groups = []): ?Response
    {
        /**
         * @var ConstraintViolationListInterface $errors
         */
        $errors = $this->get('validator')->validate($devis, $constraint, $groups);

        if (0 < $errors->count()) {
            return new JsonResponse(['errors' => $errors, 'message' => 'Les paramêtres fournis pour la requête ne sont pas valides.'], 400);
        }
        return null;
    }

     
    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            'validator' => '?'.ValidatorInterface::class,
            'translator' => '?'.TranslatorInterface::class,
            'jms_serializer' => '?'.SerializerInterface::class,
            'doctrine.orm.entity_manager' => '?'.EntityManagerInterface::class,
        ]);
    }
    public function addClient(Client $client, array $requestContent = []) :Client
    {
        if (true === isset($requestContent['client'])) {
            $client->setClient($requestContent['client']);
        }

        if (true === isset($requestContent['achats'])) {
            /** @var Produits $produits */
            $produits = $this->em->getRepository(Produits::class)->find($requestContent['achats']);
            
            $client->addAchat($produits);
        }

        if (true === isset($requestContent['montant_achat'])) {
            //1-total des produit
            $montantProduit=0;
            foreach ($requestContent['produits'] as $id) {
                $produit = $this->em->getRepository(Produits::class)->find($id);
                $montantProduit += $produit->get();
            }
            $client->setClient($montantProduit);
        }

        return $client;
    }
}
