<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Form\ProduitsType;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Validator\Validator\ValidatorInterface; 
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as SWG; 
use Nelmio\ApiDocBundle\Annotation\Model; 



class ProduitsController extends AbstractFOSRestController
{
    private $produitsRepository;
    
    
    public function __construct(ProduitsRepository $produitsRepository)
    {
        $this->produitsRepository=$produitsRepository;   
    }
    /**
     * @Rest\Get(
     * path = "/produits",
     * name = "list.produits")
     * @SWG\Response(
     *      response = 200,
     *      description = "liste des produits",
     *      @Model(type=Produits::class)
     * )
     * @SWG\Tag(name = "Produit")
     * @return View
     */
    public function cgetProduitAction(ProduitsRepository $produitsRepository)
    {
        $produits = $produitsRepository->findAll();
        return $this->view(['data' => $produits], 200);
    }

    /**
     * @Rest\Post(
     * path = "/produits/add",
     * name = "add.produits")
     * @SWG\Response(
     *      response = 200,
     *      description = "ajout de produits",
     *      @Model(type=Produit::class)
     * )
     * @SWG\Tag(name = "Produit")
     * @return View
     */
    public function postProduitAction(Request $request,ValidatorInterface $validator)
    {
        $produit = new Produits();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->submit($data);
        $errors = $validator->validate($produit);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return new Response($errorsString);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($produit);
        $em->flush();
        return $this->view(['data' => $produit], 201);
    }

    
        /**
         * @Rest\Get(
         * path = "/produits/show/{id}",
         * name = "show.produits")
         * @SWG\Response(
         *      response = 200,
         *      description = "voir un produits",
         *      @Model(type=Produit::class)
         * )
         * @SWG\Tag(name = "Produit")
         * @return View
         */
    public function getOneProduitAction(string $id, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $produit = $em->getRepository(Produits::class)->find($id);
        if (!$produit) {
            $message = $translator->trans('objet.not_found', ['%id%' => $id], 'errors');
            return new JsonResponse(['message' => $message], 404);
        }
        try {
            return $this->view(['data' => $produit], 200);
        } catch (\Exception $e) {
        }
        return new JsonResponse(null, 204); 
    }

    /**
     * @Rest\Put(
     * path = "/produits/edit/{id}",
     * name = "update.produits")
     * @SWG\Response(
     *      response = 200,
     *      description = "modifier un produits",
     *      @Model(type=Produit::class)
     * )
     * @SWG\Tag(name = "Produit")
     * @return View
     */
    public function putProduitAction(String $id,Request $request,
    ValidatorInterface $validator, EntityManagerInterface $em,
     TranslatorInterface $translator)
    {
        $produit = $em->getRepository(Produits::class)->find($id);
        if (!$produit) {
            $message = $translator->trans('objet.not_found', ['%id%' => $id], 'errors');
            return new JsonResponse(['message' => $message], 404);
        }
        try {
            $body = $request->getContent();
            $data = json_decode($body, true);
            $form = $this->createForm(ProduitsTypeType::class, $produit);
            $form->submit($data);
            $errors = $validator->validate($produit);
            if (count($errors) > 0) {
                $errorsString = (string) $errors;
                return new Response($errorsString);
            }
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        } catch (\Exception $e) {
        }
        return $this->view(['data' => $produit], 200);
    }

    
     /**
     * @Rest\Delete(
     * path = "/produit/{id}",
     * name = "remove.produit")
     * @SWG\Response(
     *      response = 200,
     *      description = "supprimer un produit",
     *      @Model(type=produit::class)
     * )
     * @SWG\Tag(name = "produit")
     * @return View
     */
    public function deleteproduitAction(String $id, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $produit = $em->getRepository(Produits::class)->find($id);
        if (!$produit) {
            $message = $translator->trans('objet.not_found', ['%id%' => $produit], 'errors');
            return new JsonResponse(['message' => $message], 404);
        }
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
            return new JsonResponse(['message'=>'l\'objet à ete supprimer avec success']);
        } catch (\Exception $e) {
        }
    }
}
