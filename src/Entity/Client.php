<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client;

    /**
     * @ORM\ManyToMany(targetEntity=Produits::class, inversedBy="clients")
     */
    private $achats;

    /**
     * @ORM\Column(type="integer")
     */
    private $montantAchat;

    public function __construct()
    {
        $this->achats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getAchats(): Collection
    {
        return $this->achats;
    }

    public function addAchat(Produits $achat): self
    {
        if (!$this->achats->contains($achat)) {
            $this->achats[] = $achat;
        }

        return $this;
    }

    public function removeAchat(Produits $achat): self
    {
        $this->achats->removeElement($achat);

        return $this;
    }

    public function getMontantAchat(): ?int
    {
        return $this->montantAchat;
    }

    public function setMontantAchat(int $montantAchat): self
    {
        $this->montantAchat = $montantAchat;

        return $this;
    }
}
