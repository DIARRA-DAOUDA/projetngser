<?php
namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Helpers\Traits\Contextable;
use JMS\Serializer\Annotation as JMS;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Helpers\Traits\TimeStampableTrait;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="username", errorPath="[username]", message="user.username.already_used", groups={"Create", "Update"})
 * @UniqueEntity(fields="email", errorPath="[email]", message="user.email.already_used", groups={"Create", "Update"})
 * @JMS\ExclusionPolicy("all")
 */
class User implements UserInterface
{
    use Contextable, TimeStampableTrait;

    public const ROLE_DEFAULT = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_AGENT';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPERVISEUR';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank(message="user.username.blank")
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank()
     * @var array
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="user.password.blank")
     * @var string The hashed password
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank(message="user.email.blank")
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": 0})
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @var bool
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $lastLogin;

    

   /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank(message="user.lastname.blank")
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank(message="user.civility.blank")
     * @var string
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @JMS\Expose()
     * @JMS\Groups({"summary", "details"})
     * @Assert\NotBlank(message="user.firstname.blank")
     * @var string
     */
    private $firstName;    
   
    
    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = [];
        $this->enabled = true;
        
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("array")
     * @JMS\SerializedName("roles")
     * @JMS\Groups({"summary", "details"})
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function hasRole(string $role) :bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function addRole(string $role): self
    {
        $role = strtoupper($role);

        if (($role !== static::ROLE_DEFAULT) && false === in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = [];
        foreach ($roles as $role) {
            $this->addRole($role);
        }
        return $this;
    }

    public function removeRole(string $role) :self
    {
        $role = strtoupper($role);

        if (($role !== static::ROLE_DEFAULT) && false !== ($key = array_search($role, $this->roles, true))) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): void {}

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getLastLogin() :?DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(DateTime $lastLogin) :self
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }
    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
 

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }
    
 

    
 

     
 

    

   

   

    

     
    

}
