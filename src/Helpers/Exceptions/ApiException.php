<?php

namespace App\Helpers\Exceptions;


class ApiException extends \Exception implements IException
{
    /**
     * Exception message
     *
     * @var string
     */
    protected $message = 'Unknown exception';

    /**
     * Exception code
     *
     * @var int
     */
    protected $code = 422;

    /**
     * Source filename of exception
     *
     * @var string
     */
    protected $file;

    /**
     * Source line of exception
     *
     * @var string
     */
    protected $line;

    /**
     * @var array
     */
    protected $errors;


    public function __construct($message = null, $errors = [], $code = 422)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }

        parent::__construct($message, $code);
        $this->errors = $errors;

    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
            . "{$this->getTraceAsString()}";
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    function apiException($e)
    {
        echo $e->getMessage();
    }
}


