<?php

namespace App\Helpers\Representation;

use App\Helpers\Utils\Constant;
use App\Helpers\Exceptions\ApiException;
 ;
  

class PresentationResourceApiData
{
    /*
     * data for getting data resource of api response
     * @var array|null
     */
    public $data;

    /**
     * code for getting code of api response
     * @var int
     */
    public $code;

    /**
     * message for getting message of api response
     * @var string
     */
    public $message;

    /**
     * @var array
     */
    public $meta;

    /**
     * message for getting message of api response
     * @var string
     */
    public $status;

    /**
     * PresentationResourceApiData constructor.
     *
     * @param null $data
     * @param string $message
     * @param int $code
     * @param array $errors
     * @param array $meta
     * @param $status
     * @throws ApiException
     */
    public function __construct(
        $data = null, $message= '', $code = 200, $errors = [], $meta = [], $status = '')
    {
        if(in_array($code, Constant::CODE_ERRORS)){
            throw new ApiException($message, $errors, $code);
        }

        $this->data = $data;
        $this->code = $code;
        $this->message = $message;
        $this->meta = $meta;
        $this->status = $status;
    }
}
