<?php

namespace App\Helpers\Representation;

use App\Helpers\Exceptions\ApiException;
 
 

class PresentationResourceApiError
{
    /*
     * data for getting data resource of api response
     * @var array|null
     */
    public $errors;

    /**
     * code for getting code of api response
     * @var int
     */
    public $code;

    /**
     * message for getting message of api response
     * @var string
     */
    public $message;

    public $data;

    public function __construct(ApiException $e = null, $errors = [], $message= '', $code = 0)
    {
        $this->errors = $errors;
        $this->message = $message;
        $this->code = $code;
        $this->data = null;
        if(!is_null($e)){
            $this->errors = $e->getErrors();
            $this->message = $e->getMessage();
            $this->code = $e->getCode();
        }
    }
}
