<?php
namespace App\Helpers\Traits;


/**
 * Trait Contextable
 * @package App\Helpers\Traits
 */

trait Client
{
    public function clientRequest($path, $authorization = null, $data = null, $method = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->container->getParameter('core_api_path').$path);

        switch (strtoupper($method)) {
                case 'GET':
                    curl_setopt($ch, CURLOPT_HTTPGET, true);
                    break;
                case 'POST':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    break;
                case 'PUT':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    break;
            }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer '.$authorization,
            ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLINFO_HTTP_CODE, true);

        $response = curl_exec($ch);
        $content = json_decode($response, true);

        return $content;
    }

    public function clientRequest1($method = 'GET', $path, $headerOptions = [], $data = null, $host = null)
    {
        if ($host === null or $host === '') {
            $host = $this->container->getParameter('core_api_path');
        } else {
            $host = $this->container->getParameter($host);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host.$path);
        switch (strtoupper($method)) {
                case 'GET':
                    curl_setopt($ch, CURLOPT_HTTPGET, true);
                    break;
                case 'DELETE':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;
                case 'POST':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    break;
                case 'PUT':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    break;
            }
        $authorization = isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : '';
        $options = [
                'Accept: application/json',
                'Content-Type: application/json', ];
        // if ($authorization) {
        //     $options[] = 'Authorization: '.$authorization;
        // }
        if ($headerOptions) {
            foreach ($headerOptions as $key => $option) {
                $options[] = "$key: $option";
            }
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $options);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLINFO_HTTP_CODE, true);
        $response = curl_exec($ch);

        $content = json_decode($response, true);

        return $content;
    }

    public function headers($request)
    {
        $headers = [
        'X-Cabinet-Key' => $request->headers->get('X-Cabinet-key'),
        'X-Customer-Key' => $request->headers->get('X-Customer-key'),
        'X-Customer-Folder-Key' => $request->headers->get('X-Customer-Folder-Key'),
        'X-Api-Key' => $request->headers->get('X-Api-Key'),
        //'x-accept-version' => $request->headers->get('x-accept-version'),
        'Authorization' => $request->headers->get('Authorization'),
    ];

        return $headers;
    }
}