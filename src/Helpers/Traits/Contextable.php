<?php
namespace App\Helpers\Traits;

use JMS\Serializer\SerializationContext;

/**
 * Trait Contextable
 * @package App\Helpers\Traits
 */

trait Contextable
{
    public static function createSerializationContext(array $context): ?SerializationContext
    {
        return SerializationContext::create()->setGroups($context);
    }
}