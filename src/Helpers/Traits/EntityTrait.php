<?php

namespace App\Helpers\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Expose;

/*
 * @ExclusionPolicy("all")
*/

trait EntityTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Serializer\Until("v1")
     * @Expose
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime",nullable=true)
     * @Serializer\Until("v1")
     * @Expose
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @Serializer\Until("v1")
     * @Expose()
     */
    private $deletedAt;

    public function hydrate(array $donnees, $em = null)
    {
        // on fait une boucle avec le tableau de données
        foreach ($donnees[0] as $key => $value) {
            // on récupère le nom des setters correspondants
            // si la clef est placesTotales son setter est setPlacesTotales
            // il suffit de mettre la 1ere lettre de key en Maj et de le préfixer par set
            $method = str_replace('_', '', ucwords('set'.ucfirst($key), '_'));
            $method[0] = strtolower($method[0]);
            // on vérifie que le setter correspondant existe
            if (method_exists($this, $method) && $value != null) {
                if ($em && is_object($value) && property_exists(get_class($value), 'id')) {
                    $value = $em->find(get_class($value), $value->getId()) ?: $value;
                }
                // si il existe, on l'appelle
                $this->$method($value);
            }
        }
    }

    public function getField($field)
    {
        // on récupère le nom des setters correspondants
        // si la clef est placesTotales son setter est setPlacesTotales
        // il suffit de mettre la 1ere lettre de key en Maj et de le préfixer par set
        $method = 'get'.ucwords($field);
        // on vérifie que le setter correspondant existe
        $result = $this;
        if (method_exists($this, $method)) {
            // si il existe, on l'appelle
            $result = $this->$method();
        }

        return $result;
    }

    public function getValueByAttribute($field)
    {
        $field = str_replace('_', '', ucwords('get'.ucfirst($field), '_'));
        $method = $field;
        $method[0] = strtolower($method[0]);
        $result = $this;
        if (method_exists($this, $method)) {
            $result = $this->$method();
        }

        return $result;
    }

    /**
     * Get the value of createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set the value of deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return self
     */
    public function setDeletedAt(\DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
