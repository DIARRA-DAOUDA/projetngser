<?php

namespace App\Helpers\Utils;
 
use App\Helpers\Exceptions\ApiException;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Helpers\Representation\PresentationResourceApiData;
use App\Helpers\Representation\PresentationResourceApiError;

class Client
{
    /**
     * @var string
     */
    private $coreApiPath;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * Client constructor.
     *
     * @param string $coreApiPath
     * @param RequestStack $requestStack
     */

    public function __construct(RequestStack $requestStack, string $coreApiPath)
    {
        $this->coreApiPath = $coreApiPath;
        $this->requestStack = $requestStack;
    }

    /**
     * Requests with Get by providing url.
     *
     * @param $url
     * @param array $headerOptions
     * @param $host
     *
     * @return PresentationResourceApiData|mixed
     */
    public function get($url, $headerOptions = [], $host = null)
    {
        try { 
            $response = $this
                ->requestApi(
                    'GET',
                    $url,
                    $headerOptions,
                    null,
                    $host
                );
        } catch (ApiException $e) {
            $response = new PresentationResourceApiError(null, $e->getErrors(), $e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response = new PresentationResourceApiError(null, [], $e->getMessage(), $e->getCode());
        }

        return $response;
    }

    /**
     * Requests with Get and provides url.
     *
     * @param $url
     * @param $headerOptions
     * @param $data
     * @param $host
     *
     * @return mixed
     */
    public function post($url, $headerOptions = [], $data = [], $host = null)
    {
        try {
            $response = $this->requestApi('POST', $url, $headerOptions, $data, $host);
        } catch (ApiException $e) {
            $response = new PresentationResourceApiError(null, $e->getErrors(), $e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response = new PresentationResourceApiError(null, [], $e->getMessage(), $e->getCode());
        }

        return $response;
    }

    /**
     * Requests with Get and provides url.
     *
     * @param $url
     * @param $headerOptions
     * @param $data
     * @param $host
     *
     * @return mixed
     */
    public function put($url, $headerOptions = [], $data = [], $host = null)
    {
        try {
            $response = $this->requestApi('PUT', $url, $headerOptions, $data, $host);
            error_log($url);
        } catch (ApiException $e) {
            $response = new PresentationResourceApiError(null, $e->getErrors(), $e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response = new PresentationResourceApiError(null, [], $e->getMessage(), $e->getCode());
        }

        return $response;
    }

    /**
     * Requests with delete and provides url.
     *
     * @param $url
     * @param array $headerOptions
     * @param $host
     *
     * @return PresentationResourceApiData|mixed
     */
    public function del($url, $headerOptions = [], $host = null)
    {
        try {
            $response = $this->requestApi('DELETE', $url, $headerOptions, null, $host);
        } catch (ApiException $e) {
            $response = new PresentationResourceApiError(null, $e->getErrors(), $e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            $response = new PresentationResourceApiError(null, [], $e->getMessage(), $e->getCode());
        }

        return $response;
    }

    /**
     * Requests with Get and provides url.
     *
     * @param $method
     * @param $url
     * @param array $headerOptions
     * @param null $data
     * @param null $host
     *
     * @return PresentationResourceApiData|mixed
     *
     * @throws ApiException
     * @throws \Exception
     */
    public function requestApi(
        $method,
        $url,
        $headerOptions = [],
        $data = null,
        $host = null
    ) {
        try {
            $result = $this
                ->clientRequest(
                    strtoupper($method),
                    $url,
                    $headerOptions,
                    json_encode($data),
                    $host
                );

            $result = new PresentationResourceApiData(
                isset($result['data']) ? $result['data'] : null,
                isset($result['message']) ? $result['message'] : '',
                isset($result['code']) ? $result['code'] : 200,
                isset($result['errors']) ? $result['errors'] : [],
                isset($result['meta']) ? $result['meta'] : [],
                isset($result['status']) ? $result['status'] : []
            );
        } catch (ApiException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }

        return $result;
    }

    /**
     * @param $path
     * @param null $data
     * @param string $method
     * @param $host
     * @param array $headerOptions
     *
     * @return mixed
     */
    public function clientRequest($method = 'GET', $path, $headerOptions = [], $data = null, $host = null)
    {
        if (!$host) {
            $host = $this->coreApiPath;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host . $path);
        switch (strtoupper($method)) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }
        $authorization = isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : '';
        $options = [
            'Accept: application/json',
            'Content-Type: application/json',];

        if ($authorization) {
            $options[] = 'Authorization: ' . $authorization;
        }
        if (empty($headerOptions)) {
            $request = $this->requestStack->getCurrentRequest();
            $headerOptions = [
                'X-Customer-Folder-Key' => $request->headers->get('X-Customer-Folder-key'),
                'X-Cabinet-Key' => $request->headers->get('X-Cabinet-key'),
                'X-Customer-Key' => $request->headers->get('X-Customer-key'),
                'X-Api-Key' => $request->headers->get('X-Api-Key')
            ];
        }
        foreach ($headerOptions as $key => $option) {
            $options[] = "$key: $option";
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $options);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLINFO_HTTP_CODE, true);
        $response = curl_exec($ch);
        $content = json_decode($response, true);

        return $content;
    }
}
