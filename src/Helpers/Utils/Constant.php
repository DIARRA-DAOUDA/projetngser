<?php

namespace App\Helpers\Utils;


use Symfony\Component\HttpFoundation\Response;

class Constant
{

    const PERIOD_TYPE = ['DAY', 'WEEK', 'MONTHLY', 'QUARTERLY', 'BIANNUAL'];
    const AWS_BUCKET_AKIL_FILES = 'https://s3-us-west-2.amazonaws.com/akil.files/maComptaFiles';
    const AWS_BUCKET_AKIL_CAB_GED = 'https://s3-us-west-2.amazonaws.com/akilcab.ged';

    const MONTH_FR = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
    const MONTH_EN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    const ACCOUNT_TYPE = [
        'GENERAL' => 'GENERALE',
        'CAPITAL' => '1-CAPITAUX',
        'IMMOBILISATION' => '2-IMMOBILISATIONS',
        'STOCK' => '3-STOCKS',
        'FOURNISSEUR' => '4-FOURNISSEURS',
        'CLIENT' => '4-CLIENTS',
        'AUTRE_TIER' => '4-AUTRES TIERS',
        'FINANCIER' => '5-FINANCIERS',
        'CHARGE' => '6-CHARGES',
        'PRODUIT' => '7-PRODUITS',
        'COMPTE_SPECIAL' => '8-COMPTES SPECIAUX',
        'ENGAGEMENT_EXTRA' => '9-ENGAGEMENTS EXTRA',
        'PERSONNALISE' => 'PERSONNALISE',
        'PERSONNEL' => 'PERSONNEL',
        'ORGANISME_SOCIAL' => 'ORGANISME SOCIAUX'
    ];

    const BALANCE_TYPE = [
        'SIMPLE' => 'SIMPLE',
        'BALANCE_N_N1' => 'BALANCE N/N-1',
        'VENTILE_PAR_MOIS' => 'VENTILE PAR MOIS'
    ];

    const CODE_ERRORS = [
        Response::HTTP_BAD_REQUEST,
        Response::HTTP_NOT_FOUND,
        Response::HTTP_METHOD_NOT_ALLOWED,
        Response::HTTP_UNPROCESSABLE_ENTITY,
        Response::HTTP_INTERNAL_SERVER_ERROR,
        Response::HTTP_PAYMENT_REQUIRED
    ];

    const CODE_CHAINE = '0000000000000000';
    const CODE_CHAINE_DISPLAY = '000000';
    const TYPES = ['INDIVIDUAL' => 'Individuel', 'COLLECTIVE' => 'Collectif'];

    const DEFAULT_DIRECTORIES_NAME = ['Ventes', 'Achats', 'Salaires', 'Trésoreries', 'Autres'];
    const DEFAULT_DIRECTORIES_DEP = ['vente' => 'Ventes', 'achat' => 'Achats', 'operations_diverses' => 'Salaires', 'banque' => 'Trésoreries', 'caisse' => 'Trésoreries', 'autres' => 'Autres'];

    const REF_ACCOUNTING_FIRM = ['DEFAULT' => 1];
    const PLAN = ['DEFAULT' => 'DEFAULT', 'ACCOUNTING_FIRM' => 'ACCOUNTINGFIRM', 'CUSTOMER' => 'CUSTOMER'];

    /**
     * =================================================================================================================
     * DEFAULT ATTRIBUTES FOR PRESENTATION RESOURCE
     * =================================================================================================================
     */

    const DEFAULT_ACCOUNT_ATTRIBUTES = ['id', 'class', 'sub_class', 'sub_class_code', 'code', 'displayed_code',
        'label', 'is_individual', 'is_used', ['rate_tva', ['id', 'label', 'rate', 'selected']]];

    const DEFAULT_JOB_ATTRIBUTES = ['id', 'name', 'file_name', 'import_state', 'action_state', 'plan_type',
        'accounting_firm_id', 'customer_folder_id', 'fiscal_year_id', 'user_id', 'user_first_name', 'user_last_name',
        'plan_name', 'nb_treated_lines', 'nb_wrong_lines', 'nb_valid_lines', 'code_mi'];

    const DEFAULT_RATE_TVA_ATTRIBUTES = ['id', 'label', 'rate', 'selected'];

    const DEFAULT_ACCOUNTS_CHART_ATTRIBUTES = ['id', 'label', 'plan_type', 'customer_folder_id', 'accounting_firm_id', ['plan_parent', ['id', 'label']]];

    const DEFAULT_OPERATION_ROW_ATTRIBUTES = ['id', 'operation_date', 'num_piece', ['account', ['id', 'class', 'sub_class', 'sub_class_code', 'code', 'displayed_code', 'label', 'is_individual']], 'reference', 'label',
        'debit', 'credit', 'delay', 'is_rate_draft', 'comment', 'lettering', 'solde', 'solde_debit', 'solde_credit',
        'code_journal'];

    const DEFAULT_PERIOD_ATTRIBUTES = ['id', 'period', 'code', 'open', 'start_date', 'end_date'];

    const IMPORT_STATE = ['INITIAL_STATE', 'CANCEL', 'SUCCESS', 'WARNING', 'ERROR', 'IN_RUNNING'];

    const IMPORT_ERROR = ['DUPLICATE_NUMBER', 'LABEL_EMPTY', 'NUMBER_EMPTY', 'INVALID_NUMBER_FORMAT'];

    const IMPORT_ACTION_STATE = ['IMPORT_CANCEL', 'IMPORT_VALIDATE', 'IMPORT_PARTIALLY_VALIDATE', 'IMPORT_DELETE', 'IMPORT_CORRECTED_VALIDATE'];

    const LAST_ACCOUNTING_PERIOD = ['period' => 'Periode 13', 'code' => 'Periode 13'];

    const ACCOUNT_TYPE_FR_LABELS = [
        'GENERAL',
        'CAPITAL',
        'IMMOBILISATION',
        'STOCK',
        'AUTRE_TIER',
        'CLIENT',
        'FOURNISSEUR',
        'FINANCIER',
        'CHARGE',
        'PERSONNALISE',
        'PERSONNEL',
        'ORGANISME_SOCIAL'
    ];

    const BILLING_USER_STATUS = [
        'BILLING_USER_IS_BLOCKED' => 'BILLING_USER_IS_BLOCKED',
        'PAYMENT_IS_REQUIRED' => 'PAYMENT_IS_REQUIRED',
        'BILLING_IS_OK'=> 'BILLING_IS_OK'
    ];

    const BILLING_MESSAGE_TYPES = [
        'BILLING_USER_IS_BLOCKED' => "Vos accès ont été restreints pour faute de paiement.
                 Merci de procéder au paiement de votre facture en attente.
                 Si tel n'est pas le cas merci de contacter le support
                 (akilcab@akiltechnologies.com / tel +225 22 42 74 06)"
    ];
    const HTTP_STATUS = [
        'PAYMENT_IS_REQUIRED'=> 'PAYMENT_IS_REQUIRED'
    ];

}
