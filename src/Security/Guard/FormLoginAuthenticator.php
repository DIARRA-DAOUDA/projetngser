<?php
namespace App\Security\Guard;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\ArrayTransformerInterface;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @package App\Security\Guard
 */
class FormLoginAuthenticator extends AbstractGuardAuthenticator
{

    private $em;
    private $encoder;
    private $jwtManager;
    private $validator;
    private $arrayTransformer;
    private $translator;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $jwtManager, ValidatorInterface $validator, ArrayTransformerInterface $arrayTransformer, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->jwtManager = $jwtManager;
        $this->validator = $validator;
        $this->arrayTransformer = $arrayTransformer;
        $this->translator = $translator;
    }
    
    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::supports()
     */
    public function supports(Request $request): bool
    {
        return true === $request->isMethod('POST') && 'app_authentication_tokens_create' === $request->attributes->get('_route');
    }
    
    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface::start()
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        $headers = ['WWW-Authenticate' => 'Bearer'];

        if (null !== $authException) {
            $message = $this->translator->trans($authException->getMessageKey(), $authException->getMessageData(), 'security');
        } else {
            $message = $this->translator->trans('authentication.bad_credentials', [], 'errors');
        }

        return new JsonResponse(['message' => $message], 401, $headers);
    }
    
    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::getCredentials()
     */
    public function getCredentials(Request $request): array
    {
        $credentials = json_decode($request->getContent(), true);
        $errors = $this->validator->validate($credentials, new Assert\Collection([
            'user' => new Assert\Required(new Assert\Collection([
                'username' => new Assert\Required(new Assert\NotBlank()),
                'password' => new Assert\Required(new Assert\NotBlank()),
            ]))
        ]));

        if (0 < $errors->count()) {
            throw new CustomUserMessageAuthenticationException($this->translator->trans('authentication.bad_request', [], 'errors'));
        }

        return $credentials;
    }

    /**
     * @inheritDoc
     * @throws \Throwable
     *@see \Symfony\Component\Security\Guard\AuthenticatorInterface::getUser()
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        $lastError = null;

        try {
            $identity = $credentials['user'] ?? [];
            if (true === isset($identity['username'])) {
                try {
                    return $userProvider->loadUserByUsername($identity['username']);
                } catch (UsernameNotFoundException $e) {
                    $lastError = null;
                }
            }
        } catch (\Throwable $e) {
            $lastError = $e;
        }
        
        throw $lastError ?? new BadCredentialsException();
    }

    /**
     * @inheritDoc
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::checkCredentials()
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $identity = $credentials['user'] ?? [];

        return $this->encoder->isPasswordValid($user, $identity['password'] ?? '');
    }

    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationSuccess()
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $token->getUser();
        $user->setLastLogin(new \DateTime());
        $this->em->flush();

        /**
         *
         * @var \Lcobucci\JWT\Token $accessToken
         */
        $accessToken = (new Parser(new JoseEncoder()))->parse($this->jwtManager->create($user));

        /**
         * @var \Lcobucci\JWT\Token\DataSet $dataSet
         */
        $dataSet = $accessToken->headers();
        
        return new JsonResponse([
            'accessToken' => [
                'id' => $accessToken->toString(),
                'issuedAt' => date('c', $dataSet->get('iat', time())),
                'expiresAt' => date('c', $dataSet->get('exp', time())),
            ],
            'user' => $this->arrayTransformer->toArray($user, User::createSerializationContext(['details']))
        ], 201);
    }
    
    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::onAuthenticationFailure()
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): JsonResponse
    {
        $headers = ['WWW-Authenticate' => 'Bearer'];
        
        if (null !== $exception) {
            $message = $this->translator->trans($exception->getMessageKey(), $exception->getMessageData(), 'security');
        } else {
            $message = $this->translator->trans('authentication.bad_credentials', [], 'errors');
        }

        return new JsonResponse(['message' => $message], 401, $headers);
    }

    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\Security\Guard\AuthenticatorInterface::supportsRememberMe()
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }
}
