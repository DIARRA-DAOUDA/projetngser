<?php

namespace App\Service;

class CalculService
{

    /**
     * Cacul la remise globale du devis
     *
     * @param integer $montantInitial
     * @param integer $remise
     * @return integer
     */
    public static function getMontantRemise(int $montantInitial, int $remise): int
    {							
        return ($montantInitial * $remise) / 100;
    }

    
    public static function getMontantTva(int $sommeTvaProduits){
        return ($sommeTvaProduits * 18) /100;
    }


   
}
