<?php

namespace App\Service;



class SendEmail
{

    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;    
    }   

    public function send($from, $to, $subject, $body)
    {
        $mail = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body,"text/html");
        $type = $mail->getHeaders()->get('Content-Type');
        $type->setParameter('charset', 'utf-8');
        $this->mailer->send($mail);
    }
   
}
