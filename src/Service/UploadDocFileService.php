<?php

namespace App\Service;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class UploadDocFileService{

  private $container;

  public function __construct(ContainerInterface $container)
  { 
    $this->container = $container;
  }


    public function uploadFile(Request $request, Array $fichiers){
        
        $files = [];
        
        foreach($fichiers as $fichier){

          $file =  $request->files->get($fichier);

          if (!$file) {
            return [
            'errors' => 'Veuillez renseigner le fichier',
            'message' => 'Invalid given data',
            'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ];
         }

         $extension = $file->getClientOriginalExtension();

         if (!in_array($extension, ['doc','pdf','txt','docs'])) {
         return [
         'errors' => ['extension' => 'Type de fichier non authorisé'],
         'message' => 'Invalid given data',
         'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
         ];
         }
         $ini=substr($fichier,0,3);
         $fileName = $ini.'_'.time().'.'.$extension;

        $file->move($this->container->getParameter('document_directory'), $fileName);
        
           $data[$fichier] = $fileName;
        }
      
        return $data;
    }
}