<?php

namespace App\Tests;

use App\Entity\Pays;
use App\Entity\Client;
use App\Entity\TypeSociete;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testSomething(): void
    {
        $client = new Client();
        $pays = new Pays();
        $pays->setLibelle("CI");
        $typeSociete = new TypeSociete();
        $typeSociete->setLibelle("SA");

        $client->setRaisonSociale('SARL');
        $client->setAdresse('Abidjan Angre');
        $client->setCodePostal('BP 200');
        $client->setVille('Abidjan');
        $client->setPays($pays);
        $client->setTypeSociete($typeSociete);
        
        $this->assertEquals($pays, $client->getPays());
    }
}
