<?php

namespace App\Tests;

use App\Entity\TypeProduit;
use PHPUnit\Framework\TestCase;

class TypeProduitTest extends TestCase
{
    public function testSomething(): void
    {
        $typeProduit = new TypeProduit();
        $libelle="pc";
        $typeProduit->setLibelle($libelle);
        $this->assertEquals("pc", $typeProduit->getLibelle());
    }
}
